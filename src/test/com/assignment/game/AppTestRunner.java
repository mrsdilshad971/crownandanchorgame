package com.assignment.game;

import com.assignment.game.tests.GameTest;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class AppTestRunner {

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(GameTest.class);
        int failed = result.getFailureCount();
        int total = result.getRunCount();
        int ignored = result.getIgnoreCount();
        System.out.printf("Total test: %d, ignored: %d, failed: %d\n", total, ignored, failed);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
    }
}
