package com.assignment.game.tests;

import com.assignment.game.DiceValue;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class DiceValueTest {

    @Test
    public void getRandomTest() {
	//DiceValue.getRandom() never returns SPADE
        List<DiceValue> randomValues = new ArrayList<>();
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());
        randomValues.add(DiceValue.getRandom());

        Assert.assertTrue("Random values should contain SPADE",
                randomValues.contains(DiceValue.SPADE));
    }

    @Test
    public void toStringTest() {
        String value = DiceValue.CROWN.toString();
        String value1 = DiceValue.CROWN.toString(DiceValue.CROWN);
        Assert.assertEquals(value1, value);

        value = DiceValue.ANCHOR.toString();
        value1 = DiceValue.ANCHOR.toString(DiceValue.ANCHOR);
        Assert.assertEquals(value1, value);
    }
}
