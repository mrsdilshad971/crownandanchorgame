package com.assignment.game.tests;

import com.assignment.game.Player;
import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {

    @Test
    public void balanceExceedsLimitTest() {
        //balanceExceedsLimit() should return true if balance is greater than or equal to limit,
        //but if balance is equal to limit, then it returns false.

        Player player = new Player("Fred", 100);
        player.setLimit(50);
        boolean isExceed = player.balanceExceedsLimit();
        Assert.assertEquals(true, isExceed);

        player.setLimit(100);
        isExceed = player.balanceExceedsLimit();
        Assert.assertEquals(true, isExceed);
    }

    @Test
    public void balanceExceedsLimitByTest() {
        //balanceExceedsLimitBy() should return true if balance is greater than or equal to
        // sum of amount and limit, but if balance is equal to the sum of amount and limit,
        // then it returns false.

        Player player = new Player("Fred", 100);
        player.setLimit(50);
        boolean isExceed = player.balanceExceedsLimitBy(30);
        Assert.assertEquals(true, isExceed);

        isExceed = player.balanceExceedsLimitBy(60);
        Assert.assertEquals(false, isExceed);

        isExceed = player.balanceExceedsLimitBy(50);
        Assert.assertEquals(true, isExceed);
    }
}
