package com.assignment.game.tests;

import com.assignment.game.Dice;
import com.assignment.game.DiceValue;
import com.assignment.game.Game;
import com.assignment.game.Player;
import java.util.List;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class GameTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

     // As roll() method does not change dice value, so we will end up with same starting value 	
    @Test
    public void playRoundDiceValueTest() {
        Player player = buildPlayer();
        Dice dice1 = new Dice();
        Dice dice2 = new Dice();
        Dice dice3 = new Dice();

        DiceValue value1 = dice1.getValue();
        DiceValue value2 = dice2.getValue();
        DiceValue value3 = dice3.getValue();

        Game game = new Game(dice1, dice2, dice3);
        DiceValue pick = DiceValue.getRandom();
        int winings = game.playRound(player, pick, 5);

	//After each round dice value should be changed, but it doesn't
        List<DiceValue> values = game.getDiceValues();
        Assert.assertEquals(value1, values.get(0));
        Assert.assertEquals(value2, values.get(1));
        Assert.assertEquals(value3, values.get(2));
    }
    //If player wins then balance does not add correctly
    @Test
    public void playRoundTest() {
        Player player = buildPlayer();
        Game game = buildGame();
        DiceValue pick = DiceValue.getRandom();
        int winings = game.playRound(player, pick, 5);

        if (winings > 0) {
            Assert.assertEquals(100 + winings, player.getBalance());
        } else {
            Assert.assertEquals(95, player.getBalance());
        }
    }

    @Test
    public void playRoundPlayerNullShouldReturnExceptionTest() {
        Player player = buildPlayer();
        Game game = buildGame();
        DiceValue pick = DiceValue.getRandom();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Player cannot be null.");
        int winings = game.playRound(null, pick, 5);
    }

    @Test
    public void playRoundPickNullShouldReturnExceptionTest() {
        Player player = buildPlayer();
        Game game = buildGame();
        DiceValue pick = DiceValue.getRandom();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Pick cannot be negative.");
        int winings = game.playRound(player, null, 5);
    }

    @Test
    public void playRoundBetNegativeShouldReturnExceptionTest() {
        Player player = buildPlayer();
        Game game = buildGame();
        DiceValue pick = DiceValue.getRandom();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Bet cannot be negative.");
        int winings = game.playRound(player, pick, -5);
    }

    private Player buildPlayer() {
        Player player = new Player("Peter", 100);
        player.setLimit(0);
        return player;
    }

    private Game buildGame() {
        Dice dice1 = new Dice();
        Dice dice2 = new Dice();
        Dice dice3 = new Dice();
        return new Game(dice1, dice2, dice3);
    }
}
