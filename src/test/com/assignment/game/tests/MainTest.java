package com.assignment.game.tests;

import com.assignment.game.Main;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void playGameTest() throws Exception {
        ByteArrayInputStream in = new ByteArrayInputStream("q".getBytes());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;

        System.setIn(in);
        System.setOut(ps);

        Main.main(null);
        String message = baos.toString();
        String[] lines = message.split("\n");
        String line = lines[lines.length - 4];
        String numberStr = line.split(" ")[7];
        int balance = Integer.parseInt(numberStr);

        System.setIn(oldIn);
        System.setOut(oldOut);

        Assert.assertTrue("Balance should reach limit",
                balance == 0 || balance >= 200);
    }

    @Test
    public void winRatioTest() throws Exception {
        //After finish the game player balance does not become equals to

        // the limit. After running main method, we can see the console for output
        ByteArrayInputStream in = new ByteArrayInputStream("q".getBytes());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;

        System.setIn(in);
        System.setOut(ps);

        Main.main(null);
        String message = baos.toString();
        String[] lines = message.split("\n");
        String ratioLine = lines[lines.length - 1];
        String numberStr = ratioLine.split(" ")[4];
        double winRatio = Double.parseDouble(numberStr.substring(0, numberStr.length() - 1));
        double ratioDiff = Math.abs(winRatio - 42.0);

        System.setIn(oldIn);
        System.setOut(oldOut);

        Assert.assertTrue("Win ratio should be near 42%", ratioDiff < 1);
    }
}
