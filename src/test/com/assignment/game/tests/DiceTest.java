package com.assignment.game.tests;

import com.assignment.game.Dice;
import com.assignment.game.DiceValue;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class DiceTest {

    @Test
    public void rollTest() {
        //If we roll the dicees,always get the same combination, for example, if in first turn
        // it's Crwon,Anchor,Diamond, in all the next turns it's Crown, Anchor and Diamond
        Dice dice = new Dice();
        DiceValue value = dice.getValue();
        List<DiceValue> rolledValues = new ArrayList<>();

        dice.roll();
        rolledValues.add(dice.getValue());
        dice.roll();
        rolledValues.add(dice.getValue());
        dice.roll();
        rolledValues.add(dice.getValue());
        dice.roll();
        rolledValues.add(dice.getValue());
        dice.roll();
        rolledValues.add(dice.getValue());

        Assert.assertTrue("rolled values should not always give same combination",
                notContailAll(rolledValues, value));
    }

    private boolean notContailAll(List<DiceValue> values, DiceValue value) {
        for (DiceValue val : values) {
            if (!val.equals(value))
                return true;
        }
        return false;
    }
}
