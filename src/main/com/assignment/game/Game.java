package com.assignment.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Game {

    private List<Dice> dices;
    private List<DiceValue> values;

    public Game(Dice die1, Dice die2, Dice die3) {
        if (die1 == null || die2 == null || die3 == null) {
            throw new IllegalArgumentException("Dice cannot be null.");
        }
        dices = new ArrayList<Dice>();
        dices.add(die1);
        dices.add(die2);
        dices.add(die3);
        values = new ArrayList<DiceValue>();
    }

    public List<DiceValue> getDiceValues() {
        values.clear();
        for (Dice d : dices) {
            values.add(d.getValue());
        }
        return Collections.unmodifiableList(values);
    }

    public int playRound(Player player, DiceValue pick, int bet) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null.");
        }
        if (pick == null) {
            throw new IllegalArgumentException("Pick cannot be negative.");
        }
        if (bet < 0) {
            throw new IllegalArgumentException("Bet cannot be negative.");
        }

        player.takeBet(bet);

        int matches = 0;
        for (Dice d : dices) {
            d.roll();
            if (d.getValue().equals(pick)) {
                matches += 1;
            }
        }

        int winnings = matches * bet;

        if (matches > 0) {
            player.withdrawBet(bet);
            player.receiveWinnings(winnings);
        }
        return winnings;
    }

}
